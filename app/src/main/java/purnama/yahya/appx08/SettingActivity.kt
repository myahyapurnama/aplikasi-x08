package purnama.yahya.appx08

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_setting.*

class SettingActivity : AppCompatActivity(), View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    //inisiasi variabel
    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"
    var BG_TITLE_COLOR = "bg_title_color"
    var FONT_TITLE_COLOR = "font_title_color"
    var FONT_TITLE_SIZE = "font_title__size"
    var TITLE_TEXT = "title_text"
    var DETAIL_FONT_SIZE = "detail_font_size"
    var DETAIL_TEXT = "detail_text"
    val DEF_TITLE_COLOR = "RED"
    val DEF_FONT_TITLE_COLOR  = "WHITE"
    val DEF_FONT_TITLE_SIZE = 24
    val DEF_TITLE_TEXT = "ASSASIN'S CREED"
    val DEF_DETAIL_FONT_SIZE = 18
    val DEF_DETAIL_TEXT =
        "In 1492 Andalusia, during the Granada War, Aguilar de Nerha is accepted into the Assassins Brotherhood. He is assigned to " +
                "protect Prince Ahmed de Granada from the Knights Templar. In 1986, adolescent Callum &quot;Cal&quot; Lynch finds " +
                "his mother killed by his father, Joseph, a modern-day Assassin. Gunmen led by Alan Rikkin, CEO of the Templars' " +
                "Abstergo Foundation, arrive to capture Joseph, who convinces his son to escape.  In 2016, Cal is sentenced to death for " +
                "murdering a pimp, but his execution is faked by the Abstergo Foundation, which then takes him to their research facility in Madrid. He is told that the Templars " +
                "are searching for the Apple of Eden, in order to eliminate violence by using the Apple's code to control humanity's free will. Sofia, Alan's daughter and the head scientist, reveals that Cal is a descendant of Aguilar, the last person confirmed to be in possession of the Apple. She puts Cal in the Animus, a machine which allows him to relive (and the scientists to observe) Aguilar's genetic memories, so that Abstergo can learn what he did with the Apple.  In 15th-century Spain, Aguilar and his partner, Maria, are deployed to rescue Ahmed, who has been kidnapped by the Templar Grand Master Tomas de Torquemada, to coerce Ahmed's father, Sultan Muhammad XII, to surrender the Apple."
    var FontTitleColor: String = ""
    var BGTitleColor: String = ""
    val arrayBGTitleColor = arrayOf("BLUE", "YELLOW", "GREEN", "BLACK")
    lateinit var adapterSpin: ArrayAdapter<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        //listener
        btnSimpan.setOnClickListener(this)
        rgBGHColor.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rbBlue -> FontTitleColor = "BLUE"
                R.id.rbYellow -> FontTitleColor = "YELLOW"
                R.id.rbGreen -> FontTitleColor = "GREEN"
                R.id.rbBlack -> FontTitleColor = "BLACK"
            }
        }
        adapterSpin = ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayBGTitleColor)
        spBGTitleColor.adapter = adapterSpin
        spBGTitleColor.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                BGTitleColor = adapterSpin.getItem(position).toString()
            }
        }
        //get resources from SharedPreferences
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        var spinnerselection =
            adapterSpin.getPosition(preferences.getString(BG_TITLE_COLOR, DEF_TITLE_COLOR))
        spBGTitleColor.setSelection(spinnerselection, true)
        var rgselection = preferences.getString(FONT_TITLE_COLOR, DEF_FONT_TITLE_COLOR)
        if (rgselection == "BLUE") {
            rgBGHColor.check(R.id.rbBlue)
        } else if (rgselection == "YELLOW") {
            rgBGHColor.check(R.id.rbYellow)
        } else if (rgselection == "GREEN") {
            rgBGHColor.check(R.id.rbGreen)
        } else {
            rgBGHColor.check(R.id.rbBlack)
        }
        sbFTitle.progress = preferences.getInt(FONT_TITLE_SIZE, DEF_FONT_TITLE_SIZE)
        sbFTitle.setOnSeekBarChangeListener(this)
        sbFDetail.progress = preferences.getInt(DETAIL_FONT_SIZE, DEF_DETAIL_FONT_SIZE)
        sbFDetail.setOnSeekBarChangeListener(this)
        edJudul.setText(preferences.getString(TITLE_TEXT, DEF_TITLE_TEXT))
        edDetail.setText(preferences.getString(DETAIL_TEXT, DEF_DETAIL_TEXT))
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnSimpan -> {
                //save configuration to SharedPreferences
                preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
                val prefEditor = preferences.edit()
                prefEditor.putString(BG_TITLE_COLOR, BGTitleColor)
                prefEditor.putString(FONT_TITLE_COLOR, FontTitleColor)
                prefEditor.putInt(FONT_TITLE_SIZE, sbFTitle.progress)
                prefEditor.putInt(DETAIL_FONT_SIZE, sbFDetail.progress)
                prefEditor.putString(TITLE_TEXT, edJudul.text.toString())
                prefEditor.putString(DETAIL_TEXT, edDetail.text.toString())
                prefEditor.commit()
                Toast.makeText(this, "Setting Disimpan", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        when (seekBar?.id) {
            R.id.sbFTitle -> {

            }
            R.id.sbFDetail -> {

            }
        }
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {

    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {

    }

}