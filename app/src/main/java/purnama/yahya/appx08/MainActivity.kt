package purnama.yahya.appx08

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    //inisiasi variabel
    lateinit var preferences : SharedPreferences
    val PREF_NAME = "setting"
    var BG_TITLE_COLOR = "bg_title_color"
    var FONT_TITLE_COLOR = "font_title_color"
    var FONT_TITLE_SIZE = "font_title__size"
    var TITLE_TEXT = "title_text"
    var DETAIL_FONT_SIZE = "detail_font_size"
    var DETAIL_TEXT = "detail_text"
    val DEF_TITLE_COLOR = "RED"
    val DEF_FONT_TITLE_COLOR  = "WHITE"
    val DEF_FONT_TITLE_SIZE = 24
    val DEF_TITLE_TEXT = "ASSASIN'S CREED"
    val DEF_DETAIL_FONT_SIZE = 18
    val DEF_DETAIL_TEXT =
        "In 1492 Andalusia, during the Granada War, Aguilar de Nerha is accepted into the Assassins Brotherhood. He is assigned to " +
                "protect Prince Ahmed de Granada from the Knights Templar. In 1986, adolescent Callum &quot;Cal&quot; Lynch finds " +
                "his mother killed by his father, Joseph, a modern-day Assassin. Gunmen led by Alan Rikkin, CEO of the Templars' " +
                "Abstergo Foundation, arrive to capture Joseph, who convinces his son to escape.  In 2016, Cal is sentenced to death for " +
                "murdering a pimp, but his execution is faked by the Abstergo Foundation, which then takes him to their research facility in Madrid. He is told that the Templars " +
                "are searching for the Apple of Eden, in order to eliminate violence by using the Apple's code to control humanity's free will. Sofia, Alan's daughter and the head scientist, reveals that Cal is a descendant of Aguilar, the last person confirmed to be in possession of the Apple. She puts Cal in the Animus, a machine which allows him to relive (and the scientists to observe) Aguilar's genetic memories, so that Abstergo can learn what he did with the Apple.  In 15th-century Spain, Aguilar and his partner, Maria, are deployed to rescue Ahmed, who has been kidnapped by the Templar Grand Master Tomas de Torquemada, to coerce Ahmed's father, Sultan Muhammad XII, to surrender the Apple."

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
    override fun onStart() {
        super.onStart()
        loadSetting()
    }

    fun loadSetting(){
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        txTitle.setBackgroundColor(Color.parseColor(preferences.getString(BG_TITLE_COLOR,DEF_TITLE_COLOR)))
        txTitle.setTextColor(Color.parseColor(preferences.getString(FONT_TITLE_COLOR,DEF_FONT_TITLE_COLOR)))
        txTitle.textSize = preferences.getInt(FONT_TITLE_SIZE,DEF_FONT_TITLE_SIZE).toFloat()
        txDetail.textSize = preferences.getInt(DETAIL_FONT_SIZE,DEF_DETAIL_FONT_SIZE).toFloat()
        txTitle.setText(preferences.getString(TITLE_TEXT,DEF_TITLE_TEXT))
        txDetail.setText(preferences.getString(DETAIL_TEXT,DEF_DETAIL_TEXT))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.option_menu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemSetting ->{
                var intent = Intent(this,SettingActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
